#include <cstdlib>
#include "BSTree.h"

using namespace bt;

void f1(int i){
    std::cout<<i<<"\n";
}

int main()
{
    BSTree tree;
    unsigned n = 20;

    /*while(tree.length()<n){
        tree.add(std::rand()%100);
    }**/
    tree.add(7).add(8).add(3).add(5).add(4);
    tree.show();
    tree.leftRotate(3).show();
    tree.rightRotate(5).show();
    tree.insertToRoot(9).show();
    tree.insertToRoot(4).show();
    tree.iterator(&f1);
    //tree.del(5).show();
    // 1,4,5,10,16,17,21
    //bsTree.add(10).add(4).add(1).add(5).add(17).add(16).add(21);
    //bsTree.show();
    //std::cout<<"\n";
    //std::cout<<bsTree.testSecc(17)<<std::endl;
    //bsTree.del(10).del(17).show();

    //std::cout<< BSTree.search( 1, BSTree.getRoot() )->datum << std::endl;
    //std::cout<< BSTree.getMin()->datum<< " " << BSTree.getMax()->datum<<std::endl;

    //std::cout<<BSTree.testSecc(5)<< " " << BSTree.testPres(5);

    return 0;
}
