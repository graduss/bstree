#include "BSTree.h"

using namespace bt;

BSTree::Node::~Node(){
    if(left) delete left;
    if(right) delete right;
    if(parent){
        if(parent->left == this) parent->left = NULL;
        else parent->right = NULL;
    }

    datum = 0;
    left = right = parent = NULL;
}

BSTree::BSTree(){
    root = NULL;
    l = 0;
}

BSTree::~BSTree(){
    if(root){
        delete root;
        root = NULL;
    }
    l=0;
}

int BSTree::length() const {
    return l;
}

BSTree &BSTree::add(const int d){
    Node *n = new Node(d);
    insert(n);
    return *this;
}

BSTree::Node* BSTree::insert(Node* n){
    Node *x, *r;
    r = root;
    x = NULL;

    while(r){
        x = r;
        if(n->datum < r->datum) r = r->left;
        else if (n->datum > r->datum) r = r->right;
        else {
            delete n;
            return r;
        }
    }

    n->parent = x;
    if(!x) root = n;
    else if(n->datum < x->datum) x->left = n;
    else x->right = n;
    l++;
    return n;
}

BSTree &BSTree::insertToRoot(const int datum){
    Node *n = new Node(datum);
    n = insert(n);
    climbUp(n);
    return *this;
}

void BSTree::climbUp(Node* n){
    while(n->parent){
        if(n->parent->left == n){ rightRotate(n->parent); }
        else{ leftRotate(n->parent); }
    }
}

BSTree &BSTree::del(const int i){
    Node *x, *y, *n;
    n = search(i);

    if(n){
        if( n->left == NULL || n->right == NULL ) y = n;
        else y = successor(n);

        if (y->left) x = y->left;
        else x = y->right;

        if (x) x->parent = y->parent;

        if(y->parent == 0) root = x;
        else if (y == y->parent->left) y->parent->left = x;
        else y->parent->right = x;

        if (y != n) n->datum = y->datum;

        y->parent = y->left = y->right = NULL;
        delete y;
        l--;
    }

    return *this;
}

void BSTree::inorderTreeWalk(Node *node, int h){
    if(node){
        inorderTreeWalk(node->left, h+1);
        for(int i = h; i>0; i--){
            std::cout<<" -> ";
        }
        std::cout<<node->datum<< std::endl;
        inorderTreeWalk(node->right, h+1);
    }
}

void BSTree::show(){
    inorderTreeWalk(root);
    std::cout<<"length: "<<this->length()<<"\n";
}

BSTree::Node* BSTree::search(const int x) const {
    Node *n = root;

    while(n && x != n->datum){
        if(x < n->datum) n = n->left;
        else n = n->right;
    }

    return n;
}

int BSTree::getRoot() const {
    if(root){
        return root->datum;
    }else throw "The tree is empty";
}

BSTree::Node* BSTree::successor(const Node *n) const {
    if (n->right != 0){
        return min(n->right);
    }

    Node *ans = n->parent;
    while(ans != 0 && n == ans->right){
        n = ans;
        ans = ans->parent;
    }

    return ans;
}

BSTree::Node* BSTree::predecessor(const Node *n) const {
    if(n->left){
        return max(n->right);
    }

    Node *ans = n->parent;
    while(ans != 0 && n == ans->left){
        n = ans;
        ans = ans->parent;
    }

    return ans;
}

BSTree::Node* BSTree::min(Node *root) const {
    Node *n;

    if(root) n = root;
    else n = this->root;

    if(n == 0){
        throw "Empty";
    }

    while (n->left){
        n = n->left;
    }

    return n;
}

BSTree::Node* BSTree::max(Node *root) const {
    Node *n;

    if(root) n = root;
    else n = this->root;

    if(n == 0){
        throw "Empty";
    }

    while (n->right){
        n = n->right;
    }

    return n;
}

void BSTree::leftRotate(Node* x){
    Node *y;
    if(x->right){
        y = x->right;
        x->right = y->left;
        if(y->left){
            y->left->parent = x;
        }

        y->parent = x->parent;
        if(x->parent == NULL){ root = y; }
        else if(x == x->parent->left){ x->parent->left = y;}
        else{ x->parent->right = y; }

        y->left = x;
        x->parent = y;
    }else throw "Right child is not exist.";
}

void BSTree::rightRotate(Node *x){
    Node *y;
    if(x->left){
        y = x->left;
        x->left = y->right;
        if(y->right){ y->right->parent = x; }

        y->parent = x->parent;
        if(x->parent == NULL){ root = y; }
        else if (x == x->parent->left){ x->parent->left = y; }
        else{ x->parent->right = y; }

        y->right = x;
        x->parent = y;
    }else throw "Left child is not exist.";
}

BSTree &BSTree::leftRotate(int i){
    Node *x = search(i);
    leftRotate(x);

    return *this;
}

BSTree &BSTree::rightRotate(int i){
    Node *x = search(i);
    rightRotate(x);

    return *this;
}

bool BSTree::isEmpty() const {
    return (bool)root;
}

void BSTree::iterator(void (*fun)(int i)){
    if(fun){
        Node *n = min(root);
        while(n){
            fun(n->datum);
            n = successor(n);
        }
    }
}
