#include <iostream>
#ifndef BSTree_H
#define	BSTree_H

namespace bt{

class BSTree {
private:
    struct Node;

public:
    BSTree();
    ~BSTree();

    BSTree &add(const int datum);
    BSTree &del(const int datum);
    BSTree &insertToRoot(const int datum);
    void show();
    int length() const;
    int successor(const int i) const;
    int predecessor(const int i) const;
    int getRoot() const;
    bool isEmpty() const;
    BSTree &leftRotate(int i);
    BSTree &rightRotate(int i);
    void iterator(void (*fun)(int i));
private:
    BSTree(const BSTree& orig){};
    BSTree operator= (const BSTree &bt){};

    Node* insert(Node *n);
    void climbUp(Node *n);
    Node* successor(const Node *n) const;
    Node* predecessor(const Node *n) const;
    Node* min(Node *root) const;
    Node* max(Node *root) const;
    void leftRotate(Node *n);
    void rightRotate(Node* n);

    struct Node {
        int datum;
        Node *left, *right, *parent;
        Node(): left(NULL), right(NULL), parent(NULL) {}
        Node(const int d): datum(d), left(NULL), right(NULL), parent(NULL) {}
        ~Node();
        private:
        Node(const Node &n);
        Node &operator=(const Node &n);
    } *root;
    int l;

    //Node* remove(Node *n);
    Node* search(const int x) const;
    void inorderTreeWalk(Node *node, int height = 0);
};

}
#endif	/* BSTree_H */

